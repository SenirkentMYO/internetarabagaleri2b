﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetArabaGaleri2B
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Application["Sayac"] == null)
            {
                Application["Sayac"] = 1;
                lblSayac.Text ="Ziyaretçilerimiz : " + Application["Sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["Sayac"];
                sayi++;
                Application["Sayac"] = sayi;
                lblSayac.Text = "Ziyaretçilerimiz : " + Application["Sayac"].ToString();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<kullanici> Kullanicilist = new List<kullanici>();
                using (SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["baglanticumlesi"].ToString()))
                {
                    baglan.Open();
                    string sorguCümlesi = "Select * from giris Where KullaniciNo='" + TextBox1.Text + "' and sifre='" + TextBox2.Text + "'";
                    System.Data.SqlClient.SqlCommand komut = new SqlCommand(sorguCümlesi, baglan);
                    komut.ExecuteNonQuery();
                    System.Data.SqlClient.SqlDataReader satir = komut.ExecuteReader();
                    while (satir.Read())
                    {
                        kullanici ar = new kullanici();
                        ar.KullaniciNo = (string)satir["KullaniciNo"];
                        ar.Sifre = (string)satir["Sifre"];
                        Kullanicilist.Add(ar);
                    }
                    baglan.Close();
                    if (Kullanicilist.Count > 0)
                    {

                        lblHata.Text = "Giriş Başarılı :)";

                    }
                    else
                    {
                        lblHata.Text = "Kullanıcı No veya Şifre Hatalı !";

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("hata;" + ex.ToString());
            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm3.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm2.aspx");
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm4.aspx");
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            Response.Redirect("WebForm5.aspx");
        }
    }
}